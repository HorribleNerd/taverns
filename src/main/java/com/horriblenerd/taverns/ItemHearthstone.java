package com.horriblenerd.taverns;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.item.UseAction;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.NBTUtil;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.Color;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import java.util.List;

public class ItemHearthstone extends Item {

    public ItemHearthstone(Properties properties) {
        super(properties);
    }

    @Override
    public int getUseDuration(ItemStack stack) {
        return 20;
    }

    @Override
    public UseAction getUseAction(ItemStack stack) {
        return UseAction.BOW;
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
        playerIn.setActiveHand(handIn);
        return ActionResult.resultConsume(playerIn.getHeldItem(handIn));
    }

    @Override
    public ActionResultType onItemUse(ItemUseContext context) {
        if (context.getWorld().getBlockState(context.getPos()).getBlock() == Registration.TAVERN_BLOCK.get()) {
            context.getItem().setTag(NBTUtil.writeBlockPos(context.getPos().up()));
            TranslationTextComponent error = new TranslationTextComponent("item.taverns.hearthstone.set");
            error.getStyle().func_240712_a_(TextFormatting.GRAY);
            context.getPlayer().sendStatusMessage(error, true);
            return ActionResultType.SUCCESS;
        }
        return ActionResultType.PASS;
    }

    @Override
    public ITextComponent getDisplayName(ItemStack stack) {
        return super.getDisplayName(stack);
    }

    @Override
    public void onCreated(ItemStack stack, World worldIn, PlayerEntity playerIn) {
        super.onCreated(stack, worldIn, playerIn);
    }

    @Override
    public boolean hasEffect(ItemStack stack) {
        return stack.hasTag();
    }

    @Override
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public ItemStack onItemUseFinish(ItemStack stack, World worldIn, LivingEntity entityLiving) {
        if (entityLiving instanceof ServerPlayerEntity) {
            ServerPlayerEntity player = (ServerPlayerEntity) entityLiving;
            CompoundNBT tag = stack.getTag();
            if (tag == null) {
                TranslationTextComponent error = new TranslationTextComponent("item.taverns.hearthstone.unset");
                error.getStyle().func_240712_a_(TextFormatting.RED);
                player.sendStatusMessage(error, true);
                return stack;
            }
            else {
                if (player.connection.getNetworkManager().isChannelOpen() && player.world == worldIn && !player.isSleeping()) {

                    BlockPos pos = NBTUtil.readBlockPos(tag);
                    entityLiving.setPositionAndUpdate(pos.getX()+0.5D, pos.getY(), pos.getZ()+0.5D);
                    entityLiving.fallDistance = 0.0F;
                    if (!player.isCreative()) {
                        ((PlayerEntity) entityLiving).getCooldownTracker().setCooldown(this, 20);
                    }
                    return stack;
                }
            }
        }
        return super.onItemUseFinish(stack, worldIn, entityLiving);
    }

}
