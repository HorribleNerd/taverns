package com.horriblenerd.taverns;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.vector.Vector3i;
import net.minecraft.util.text.StringTextComponent;

import java.util.List;
import java.util.Objects;

public class TileTavern extends TileEntity implements ITickableTileEntity {

    public TileTavern() {
        super(Registration.TILE_TAVERN.get());
    }

    @Override
    public void tick() {

        List<PlayerEntity> playerList = Objects.requireNonNull(this.getWorld()).getEntitiesWithinAABB(EntityType.PLAYER, new AxisAlignedBB(this.pos.add(-16, -16, -16), this.pos.add(16, 16, 16)), x -> true);
        for (PlayerEntity player : playerList) {
            EffectInstance instance = player.getActivePotionEffect(Effects.LUCK);
            if (instance == null) instance = new EffectInstance(Effects.LUCK, 2);
            else instance = new EffectInstance(Effects.LUCK, 2 + instance.getDuration());
            player.addPotionEffect(instance);
        }
    }
}

