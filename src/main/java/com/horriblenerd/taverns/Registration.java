package com.horriblenerd.taverns;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.Rarity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Util;
import net.minecraft.util.datafix.TypeReferences;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

import static com.horriblenerd.taverns.Taverns.MODID;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class Registration {

    private static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, MODID);
    private static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, MODID);
    private static final DeferredRegister<TileEntityType<?>> TILES = DeferredRegister.create(ForgeRegistries.TILE_ENTITIES, MODID);

    public static final RegistryObject<Block> TAVERN_BLOCK = BLOCKS.register("tavern_block", () -> new BlockTavern(AbstractBlock.Properties.create(Material.WOOD)));
    public static final RegistryObject<Item> TAVERN_BLOCK_ITEM = ITEMS.register("tavern_block", () -> new BlockItem(TAVERN_BLOCK.get(), new Item.Properties().group(ItemGroup.DECORATIONS)));
    public static final RegistryObject<Item> HEARTHSTONE_ITEM = ITEMS.register("hearthstone", () -> new ItemHearthstone(new Item.Properties().maxStackSize(1).group(ItemGroup.TOOLS).rarity(Rarity.EPIC)));
    public static final RegistryObject<TileEntityType<TileTavern>> TILE_TAVERN = TILES.register("tavern_block", () -> TileEntityType.Builder.create(TileTavern::new, TAVERN_BLOCK.get()).build(null));

    public static void init() {
        final IEventBus modBus = FMLJavaModLoadingContext.get().getModEventBus();
        BLOCKS.register(modBus);
        ITEMS.register(modBus);
        TILES.register(modBus);
    }

}
